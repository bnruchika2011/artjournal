Title: Narrative Journal
Date: 2023-02-15 12:01
Description: A free flow of how I think and do things in words and images
Category: Narrative Journal

---

## Where it all began?

<span class="part-2-color"> It was a hot summer afternoon in May. Near the end of a master's year, everyone is looking for an internship and ways to spend their year off. I was especially excited to spend my break doing something outside of my comfort zone. To a zone away from my desk-research type of practice towards "with the people", and "on the ground". After working with the Just Futures Co-lab for two semesters, I am familiar with the projects they are working on. Then there's CowKHi, a community project I've been curious about but haven't worked on directly. So, Naveen opens an invitation for an internship with other team members at Chennapatna for 2 months.</span>

<span class="part-2-color">One of the main reasons I wanted to work here is that I am fluent in Kannada. As a  skill, I badged it against my chest. And, honestly, it makes sense. My tools were prepared. Simply be aware, observe, listen, and take notes.</span>

<span class="part-2-color">I'm now a few weeks into the project. I met the other team members. I'm worried about whether I'll be able to do justice to the role. On the first day at the field. As a "problem solver," I ask each Health Navigator (HN)6 what they believe the problem is with their process/workflow. I didn't have to do that. Plus, it was very soon. I recall Naveen patting my shoulder and saying, "We are not going to do it this way yet," telling me to stop seeing things as a problem just yet. That's when I took a breather. I sat back. Allow the HNs and other members to intuitively pull me if they need anything.</span>

<span class="part-2-color">I had a set of deliverables to meet as an intern. I've been interning with HN and the other project team members in certain ways. To begin, one of the first projects I worked on was creating a "Metadata Handbook" for the daily field work of HNs. This was created in collaboration with all of the HNs and other interns like myself. What is the purpose of this manual? This Metadata book is intended to function as a toolkit for HNs. All of the HNs involved in the construction of the Channapatna Health Library have developed a set of interview guidelines, protocols, and ideas. Building the health library entails HNs speaking with residents of villages surrounding Channapatna. An audio recording is created with the informed consent of these conversations between HNs and their client. This toolkit holds this collection and is also used as a training toolkit for onboarding a new HN into the project. </span>

## Working with *ಮಾತು ಕಥೆ* (maathu kathe)

<span class="part-2-color">Listening to and transcribing field visit discussions in the form of audio recordings was a large part of my work in developing this toolkit. Audio transcription, as we called it, is a simple task. To put it another way, this method for "different ways of knowing" came to me intuitively. During a field trip, every conversation is audio recorded on phones. It is a method of storing knowledge, events, and ideas that have been discussed. The cohort discussions are mostly group discussions that follow a weekly agenda. We communicate primarily in Kannada and Urdu. As a result, the majority of these audios feature conversations in Kannada and Udru, with some in English and Hindi. Each audio has a date and, if possible, a context label.</span>

<span class="part-2-color">Through transcribing I had to "dissect" the selected audio to gain insights. To develop the Metadata Handbook, my task was to gather content in Kannada and their translations in English. The process would be to listen to selected audio and transcribe specific ideas. Make these collections of ideas into a structured piece of information. For example, making "Interview Best Practices" takes me listening to the discussion, and translating the conversation into pieces of knowledge. HNs would have intuitively talked about how to conduct better interviews. How to ask for recording consent from the interviewees? Such would be questioned to construct a content piece for "Interview Best Practices". Then, get reviews from HNs on how it might add value to the handbook and importantly, proofread the Kannada version.</span>

## Making of a good listener and sense-maker

<span class="part-2-color">This activity of working with audio files and transcribing had grown out to be one of my best qualitative skills. As the months pass in working with the community, I developed deeper relationships with the HNs and their stories too. For months, the practice of making stories with "audio" content as the primary artifact was effective. Language played a very interesting role here to fuel my interest. Kannada has been my longest-spoken language. I am a native speaker of Telugu language. I have learnt to speak, read and write Kannada by living in the state for more than 14 years. As a researcher, I find it important to hold strong communication skills. With Kannada, I tend to have a natural affinity both culturally and politically.</span>

<span class="part-2-color">This practice of transcription opens me to myself regarding my memory patterns, how I relate to people working with me, my strengths of interpersonal skills and more. For example, having worked with the community for months I was able to recognize the HNs by their voice while transcribing audio. But as an honest note, transcribing is very laborious. Given then there are no on-the-go tools which are used to process natural language, it takes a human intervention in capturing stories and connections from a conversation.</span>

<span class="part-2-color">I cannot see this practice in isolation anymore. Meaning, I would not be able to transcribe as efficiently if I weren't an active part of the community. Being in action with people actively and passively developed a tacit knowledge<sup>1</sup> of connecting multiple contexts into my transcriptions. Let me unpack this more in the further sections.</span>

## Discovering my mix of working with communities, technology and storytelling

<span class="part-2-color">Working at the Channapatna Health Library first, starting as an internship and towards forming a capstone has embarked me into new ideas in community-owned network infrastructures, digital health archives, feminist technology<sup>11</sup> and care work. Working with people and communities has always helped me ground my values and given me a sense of belonging.</span>

<span class="part-2-color">I have been defining my position through a sense-making practice that involves the stories, experiences, and relationships of the people I work with. In general, I see a pattern in my questioning that always relates to ideas of collaboration, justice, feminist practices, and critical socio-technical issues.</span>

## Some mistakes on the way

During the 3rd semester TDR(Trans-disciplinary Research) course between November to December 2022, I was intending to work on a few threads I had formed in the internship. Here, I had admittedly taken a wrong approach of forming an inquiry where I was trying to force the discipline of computational design. A wrong approach in thinking how can I bring computation into my ongoing approach rather than unpacking other critical nuances. 

> My line inquiry started off with the keyword, data visualization. I have added an objective of experimenting with computation this TDR. The idea is originating from my encounter of the term “poetic systems”. “The transformation of levels of knowledge, cognition and perception into poetic levels of meaning has always been a central theme in art—it is thus possible to make the invisible visible, the intangible tangible, to encode and decode meanings.”<sup>2</sup>.  The ideas of expressing with art and what poetic-ness exactly mean are still far fetched to me. But the theme of producing art with digital technology sounds exciting.

## Thinking with the health archive

I developed a transdisciplinarity approach to thinking at the intersections of computing, art, and archives by placing in this project space. My investigation narrows to explicitly thinking with the ongoing, living health library over the TDR course of four weeks.

My process of annotating the audio content was an act of careful observation and sense-making with the stories being narrated, real experiences, and sometimes tagging anecdotes from field visits. Through this process, I am able to imagine what HNs' in-the-field realities are: the emotional-mental labour of care work and their motivations for being in this occupation, which HNs sometimes call "Jana Seve".

Identifying this as a practice of "affective annotation", I have been able to add emotional and affective tags to the stories from the audio files of health library. These annotations serves as **self-expression**, as my individual use of emotional tags to show a **subjective experience**. 

## In search of identity and practice

This way of thinking has embarked a path into a new inquiry at the intersections of:

- **Art-based research practice**
- **Exploring digital archive and oral history**
- **Using media in art expressions**

## Further into making

Developing from my ongoing inquiry of close interactions with the content of Channapatna Health Library to capture and articulate stories of health experiences and life experiences, I have learnt how an audio archive<sup>2</sup> serves an important purpose of storing a form of oral history for the community. In attempts to answer my question of "how to visualise an audio archive", I have tried annotating by sense-making with the stories being narrated, real experiences, and sometimes tagging anecdotes from field visits and creating visuals expressing my ideas. Taking this a step further, I am curious to investigate how can I use different mediums of expression from a creative and artistic perspective. Given my interest in trying my hand at new media arts, I am interested in figuring out how computer-based interactive art can support my expression.

As I had mentioned before, I have noticed how there is a development of *qualitative* skill in interpreting audio stories. Here, is an interesting intersection at qualitative research and art expression. Both artistic practise and qualitative research practise can be viewed as cafts. Qualitative research does not simply collect and write; it composes, orchestrates, and weaves. As Valeria J. Janesick (2011) points out, the researcher, like the artist, is the instrument in qualitative research. Furthermore, both practises are holistic and dynamic, requiring reflection, description, problem formulation and solving, as well as the ability to recognise and explain intuition and creativity in the research process<sup>9</sup>.

## What oral histories in CHL?

A 18 minute long audio recording of an interview conducted my HN Mangalamma where she was interacting with an elderly women from her locality has opened me to a different dimensions of affective annotation. This archive content served as a special introduction to **oral history** for me. 

Following is an instance of my thoughts while annotating this audio:

> HN Mangalama's 18-minute audio recording captures a conversation with a 75-year-old woman. As I slowly listen, this elderly woman paints a picture of the various differences in pregnancy care today versus in the past. She is sharing with Mangalama details about delivery, food habits, and care before and after delivery. She ==acknowledges how these days pregnant women are asked to do mostly nothing, whereas earlier they were casually doing daily chores==. As the conversation progresses, the subtle ==background noise==—rather, the buzzing sounds of fast moving trucks from one side to the other, honking of two-wheeler, birds chipping, and of course, a child's constant interruption—gives me a picture of the conversation's setting. Perhaps they are right outside the door. Perhaps it's evening. And could not resist noticing a different ==Kannada accent==. This audio connects me to a ==memory from the field about how Mangalama== is well known in the group for recording the longest conversations. And a few sessions with all of the HNs to develop "interview best practices" by analysing how Mangalama prepares for such an interview.

Some questions I could sit with post construing my subjective affective annotions are:

- What are the ethics to take care of when thinking with oral histories?
- How and who can think of different ways of interacting with oral histories?
- What can be different ways of adding prespectives to these stories?

As a part of secondary research, I stumbled upon **Oral History and
Digital Humanities: Voice, Access, and Engagement** a book by Douglas A. Boyd and Mary A. Larson has validated one of my takes on oral histories. They highlight in the context of interpreting audios. Captured audio is analogous to a child separated from the rest of the family. It requires clarification, background information, and context. To accomplish this, recordings must be combined with other tellings—both gathered and personal—to reveal a more complete picture of what they mean. Stories must find their place, and once they do, they are ready for retelling. Of course, each individual approaches the recording with their own set of questions, experiences, and background. They will reintroduce the story when memory and circumstance demand it, and the occasion will reshape the story by adding another layer of meaning implicit in the 
new setting<sup>5</sup>. The keywords **multidimensional aspect** and **retelling** I want to sit with.

<span class="part-1-color">Developing from my ongoing inquiry of close interactions with the content of Channapatna Health Library to capture and articulate stories of health experiences and life experiences, I have learnt how an audio archive<sup>12</sup> serves an important purpose of storing a form of oral history for the community. In attempts to answer my question of "how to visualise an audio archive", I have tried annotating by sense-making with the stories being narrated, real experiences, and sometimes tagging anecdotes from field visits and creating visuals expressing my ideas. Taking this a step further, I am curious to investigate how can I use different mediums of expression from a creative and artistic perspective. Given my interest in trying my hand at new media arts, I am interested in figuring out how computer-based interactive art can support my expression.</span>

## Intent, position, path and actions

**Intent**: With the practise of interacting with the digital archives of Channapatna Health Library, I am intending to unpack how oral histories on health, locally situated stories, are received and interpreted by a "listener." Looking at the traditional ways in which oral histories are stored, i.e., by audio recordings, how can I rethink a way of storing these oral histories with different layers of knowledge, memories, and local contexts to be engaged with? 

**Position**: Having interacted with the audio contents, I see myself making various connections from the field discussions on the ethics of interviews, the values of making an archive, ideas around who is getting included, and my personal stance on health. Standing with an insider view, I am thinking of different ways one can enhance the engagement with the audio content. Relying on the interpersonal skills I have developed by working with the community, I want to attempt layering the audio with emotions, my personal stories around health, and values the community holds.

**Path**: Proposing the assumption that there is a lack of a platform that represents audio archives in a more non-linear way (i.e., not just audio), I am willing to experiment with auditory experiences in the context of Channapatna Health Library. The plan is imagining a small branch of database sourcing from CHL where I have illustrated a series of audio content in a specific pattern, based on a theme (example: women health, care-work by women), based on a certain issues. Can this serve as a use case of how one can interact with audio archives for a larger audience of health workers, designers, and researchers?

**Action**: My primary plan of action is taking an approach of art-based exploratory research method where I could experiment with media technologies which can help me play with digital storytelling. The second is to articulate a methodology for how to work with audio knowledge forms in collaboration with communities. My capstone can be seen unpacking an argument on how media technologies can be used to express stories with audio. A living and growing space to invite experiments to rethink how audio archives are accessed.


### References

1. Alice Yuan Zhang. “Alice Yuan Zhang,” n.d. https://aliceyuanzhang.com/.
2. Ars Electronica Futurelab. “Poetic Systems.” Accessed November 10, 2022. https://ars.electronica.art/futurelab/en/research-poetic-systems/.
3. Boeck, Angelika. “What Is Artistic Research?” w/k–Between Science & Art Journal, February 24, 2021. https://between-science-and-art.com/what-is-artistic-research/.
4. briz, nick. “Nick Briz.” nick briz, n.d. http://nickbriz.com.
5. Boyd, Douglas A. _Oral history and digital humanities: voice, access, and engagement_. Springer, 2014.
6. Engineer, Rayomand. “Going Door-To-Door, MAYA Is Making over 50,000 People in Karnataka Healthier.” The Better India, December 19, 2017. https://www.thebetterindia.com/124943/maya-health-initiative/.
7. Feminist Data Manifest-No. “Feminist Data Manifest-No,” n.d. https://www.manifestno.com.
8. Gray, Carole, and Julian Malins. _Visualizing research: A guide to the research process in art and design_. Routledge, 2016.
9. Leavy, Patricia. _Method meets art: Arts-based research practice_. Guilford Publications, 2020.
10. SFPC | School for Poetic Computation. “SFPC | School for Poetic Computation,” n.d. https://sfpc.io/.
11. Murray, Padmini Ray, Naveen Bagalkot, Siddhant Shinde, Shreyas Srivatsa, and Health Navigators Maya Health. “A ‘Feminist’ Server to Help People Own Their Own Data | THE BASTION.” THE BASTION, August 12, 2022. https://thebastion.co.in/politics-and/tech/a-feminist-server-to-help-people-own-their-own-data/.
12. O’Donoghue, Dónal. "Are we asking the wrong questions in arts-based research?." _Studies in art education_ 50, no. 4 (2009): 352-368.
13. Pelican – A Python Static Site Generator. “Pelican – A Python Static Site Generator,” n.d. https://getpelican.com/.
14. YouTube. “Ramakka Introduces Channapatna Health Library,” September 14, 2022. https://www.youtube.com/watch?v=_E49WufhvK8.
